* An Emacs configuration
This is my =~/.emacs.d= directory. My primary mode of operating is macOS GUI so things work best on that. It is also compatible (but will have more bugs) with running from a terminal, Linux, and Windows.

#+CAPTION: Emacs frame with README and Magit windows
[[file:screenshots/screen1.png]]

** Goals
*** Always fast and responsive.
Nothing should produce unnecessary lag, waiting, or clunkiness. Remote use cases like [[https://www.gnu.org/software/tramp/][TRAMP]] and =ssh= have been ironed out through heavy use.
*** The appearance should be functional and attractive.
For maximum concentration and ergonomics, there should be no extraneous information. Strip away any distracting lines or unnecessary UI elements.
*** Optimize startup time.
Slow startup is a barrier to improving this config and it just makes me feel bad. On my machine Emacs starts it up in about 1.1 seconds. If [[https://www.gnu.org/software/emacs/manual/html_node/emacs/Saving-Emacs-Sessions.html][desktop]] is enabled the actual startup time can be much longer, often more like 10 seconds. Although desktop is heavier than I would like, it works reliably and that's more than I can say for the other buffer/window/frame persistence methods I have tried.
*** All interfaces should be uniform.
[[https://github.com/abo-abo/swiper][ivy]] and [[http://company-mode.github.io/][company]] are the two main ways to find stuff.
*** Use normal Emacs key bindings, enhance where appropriate.
Then mix in the most common macOS key bindings so things like =s-c= and =s-w= work like in the rest of the OS. I work heavily with macOS applications like Mail, Calendar, Messages, MS Outlook, Firefox. So it's important that the basic key bindings in those applications have a translation in Emacs. Keep things as consistent and idiomatic as possible while merging these two disparate binding systems.
*** Extensive customization but no excess.
If I'm not using a package, it's out. But I guess I use a lot of packages. About 180 at last count. Dependencies bump it up to about 210.
*** Custom Emacs build.
Check out the =setup= script. It builds Emacs from the latest master to enjoy benefits such as performance improvements, better macOS compatibility, image viewing, modules like [[https://github.com/akermu/emacs-libvterm][libvterm]] and [[https://github.com/politza/pdf-tools][pdf-tools]], and new features like =js-jsx-mode= and Jansson support (see [[https://raw.githubusercontent.com/emacs-mirror/emacs/master/etc/NEWS][GNU Emacs NEWS]], or better yet, =M-x view-emacs-news=).
*** Reproducible and portable configuration
You should be able to get a working config by doing:
1. Clone this repo
2. Run =setup=, which will build and install Emacs
3. Start Emacs
4. Wait a couple minutes for packages to install
*** Frequent updates.
I update frequently and often pick up new functionality from Emacs master.
*** Utilize [[https://github.com/jwiegley/use-package/tree/master][use-package]] and [[https://www.gnu.org/software/emacs/manual/html_node/emacs/Packages.html][package.el]] for as much configuration as possible.
Defer loading packages until they are needed. I have tried other package managers and they work great. But they have lots of features I don't want and are slow compared to =package.el=. For the few packages outside of ELPA and MELPA, I have a custom solution in =use-package-git=. It is a very simple =use-package= extension that adds a =:git= keyword. I prefer this because I can specifically pull chosen packages via git. For the rest, =package.el=, with a little help from [[https://github.com/Malabarba/paradox/][Paradox]], works great.
** Features
*** Custom mode-line.
Inspired by [[https://github.com/itchyny/lightline.vim][vim-lightline]].
*** Custom theming system.
Quickly (and completely!) switch between light and dark, or any other themes. A key part of this is realizing that many packages have dependencies on the theme so it is not nearly enough to call `load-theme`. So there is some additional logic to automatically change as much as possible and use a hook to do the rest.
*** Editing
Highlights include
**** Clemera's [[https://github.com/clemera-dev/undo-redo][undo-redo]]
**** [[https://github.com/magnars/multiple-cursors.el][Multiple cursors]]
**** [[https://github.com/DogLooksGood/parinfer-mode][Parinfer]]
**** [[https://github.com/Fuco1/smartparens][Smartparens]] with lots of customization
*** Environment support.
macOS doesn't hand its applications much in terms of standard UNIX environment configuration. I have a customized setup to initialize the environment by running `bash --login` and pulling in its environment variables. The details are handled in my non-Emacs [[https://github.com/mnewt/dotfiles][dotfiles]].
*** Eshell.
There is nothing like Eshell. It is truly amazing the way it enables powerful new ways to handle remote systems administration. There are extensive customizations to make it seamless with the rest of the environment and UI.
*** File operations and Dired.
Make the uniquely powerful Dired more comfortable, informative, and reliable.
*** Persistence.
Persist buffers (optionally), undo, command completions, recent files, etc.
*** Navigation.
Some innovative intra- and extra- buffer navigation strategies. Check out =winner-wrong-window= for an example.
*** Version control.
[[https://magit.vc/][Magit]] and more. But mostly magit.
*** Language support.
**** Emacs Lisp of course, many enhancements.
**** Clojure and Clojurescript
**** Scheme (mostly [[https://call-cc.org/][CHICKEN]])
**** Common Lisp
**** [[https://github.com/emacs-lsp/lsp-mode][lsp-mode]]
**** Org
**** [[https://github.com/purcell/reformatter.el][reformatter]]
**** [[https://www.flycheck.org/en/latest/][flycheck]]
**** log viewing
**** docker
**** Bash and friends
**** Web, Javascript and React development
**** Python
**** Lua
**** Ruby
**** Embedded language support with [[https://github.com/polymode/polymode][polymode]] and [[https://github.com/aaronbieber/fence-edit.el][fence-edit]]
** Supported Emacs versions
*** 24.3+
*** A few features are 27.1 (master) only.
** TODO
See TODO.org for some of the things I'm working on.
** License
The Free Software Foundation may control certain pieces of this by virtue of them being contributed to Emacs or a package in ELPA or MELPA. The rest is basically in the public domain. See the LICENSE file for details.
